# Mini-projet MrSudoku

Le jeu du Sudoku en mini-projet.

## Solver
Disponible dans [solver.clj](src/mrsudoku/solver.clj)

On commence par construire une **grille des valeurs possibles** de grid avec la fonction  `possible-grid` sous la forme :`{:x :y :block :value}`
 où `value` contient l'ensemble des *valeurs possibles à entré dans cette même position* dans `grid`. Les valeurs possibles de chaques cases sont calculés dans la fonction `possible-value` qui examine dans la position courante sa **ligne, colonne et son block** pour éliminer toutes les valeurs **déjà présente** de l'ensemble des valeurs possible de cette case. Et qui donc ne peuvent être placer dans cette case.
 Comme `possible-grid` et `grid` ont la meme forme, on pourra utiliser les fonctions `cell`, `block`, `col`, `row` etc... du fichier [grid.clj](/src/mrsudoku/grid.clj) pour retrouver les cellules que l'on souhaite.
 On analyse ensuite la grille pour voir s'il y a des cases qui n'ont **qu'une seule valeur** dans sa `possible-grid` et s'il y en a, on l'écrit dans `grid` grâce à la fonction `to-set-uniq`.

Lorsqu'il n'y a plus de valeurs *"évidente"*(#valeur possible = 1), on filtre toutes les *valeurs possible* entre elles dans la fonction `lock-grid`, qui examine chaque `block` de la `possible-grid` si il y a des `valeurs` qui ne peuvent etre uniquement dans une des `lignes` ou `colonnes` du block avec la fonction `filtre-lock`. 
C'est à dire que dans un block il y a au moins une valeur qui appartient à l'ensemble des valeurs possible d'une ligne/colonne et qui n'appartient pas aux autres lignes/colonnes du `block` et donc ne peuvent etre dans les autres cases de cette ligne/colonne hors du `block` courant. On les supprime donc avec la fonction `locked` qui pour chaque cellule de la ligne/colonne fera une difference des valeurs à supprimer et des valeurs de la cellule.

On résout la grille `grid` grâce a la fonction `solve` qui boucle jusqu'à ce qu'il n'y ait plus de cellule en `:status :empty` vérifié avec la fonction `checks-cell` qui parcours la grille et renvoie `true` s'il reste des cases vides, `false` sinon. 
On fait tout d'abord appel aux fonctions `to-set-uniq` et `filtre-cells` sur `col`, `row`, `block` qui permet d'affecter à la grille les seules valeurs possible pour chaque `col`, `row`, `block` de la grille. Et lorsqu'aucun changement depuis la dernière récurrence n'est détecté, on fait des appels à la fonction `lock-grid` sur `col` et `row`.

## View

### Boutons

Les boutons sont gérés dans la fonction `mk-main-frame` de [view.clj](/src/mrsudoku/view.clj) :

* Pour le bouton `Load`, on fait appel à une bibliotheque de Java, `JFileChooser` qui permet d'ouvrir une fenetre graphique, écrite dans la fonction `tlt-get-file` et de récuperer le **nom du fichier** selectionné par l'utilisateur. On charge ensuite le fichier grâce à la fonction `vec-grid`, qui avec le nom du fichier récupere chaque ligne qui seront ensuite parse à chaque occurence d'un `espace` et génerer des `vecteur de string` de lignes du fichier. On génère ensuite une `grid` avec la fonction `gen-grid` qui placera ses valeurs, remplacera les `"."` par des cellules vides et formera les `block` grâce au séparateur `"|"`. On séparera aussi les rangées, avec les lignes qui ont un seul élément dans le vecteur. L'affichage de la grille chargé sera effectué avec un appel à `show-sudoku!` de [core.clj](/src/mrsudoku/core.clj). Je n'ai pas trouve de moyen pour fermer les fenêtres ouverte précédemment. Lorsque l'on quitte une des grilles, cela quitte le programme et donc fermera toutes les grilles ouvertes. 

* Pour le bouton `Solve`, on fait appel à la fonction `solve` pour récuperer la **grille solution** et ensuite l'inserer dans `ctrl` grâce à la fonction `update-grid-view!` du fichier [control.clj](/src/mrsudoku/control.clj) qui pour chaque cellule (hors les cellules `:init`) mettra à jour la grille de l'atom `ctrl` avec la fonction `change-cell!`.

* Pour le bouton `Quit`, on fait appel à la fonction exit de `System`


## Observations
Le solver peut résoudre certaines grilles "faciles", mais pour des problèmes plus difficiles ou grilles à plusieurs solutions possible. Le solver ne peut pas les résoudre et les grilles renvoyées pourront avoir des `conflicts` que l'on peut détecter avec `grids-conflicts` du fichier `engine`. Certaines grilles ne sont pas complète due au valeurs déja présente dans la grille, c'est à dire que l'ensemble de valeurs possible pour ces cases sans valeurs, sont vides.
Comme la resolution d'une grille n'est pas certaine, nous limitons le nombre d'essai à `100` dans la fonction `solve`.